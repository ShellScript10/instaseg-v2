import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Alert, Linking } from 'react-native';
import { Camera as RNCamera } from 'expo-camera';
import * as ImageManipulator from 'expo-image-manipulator';

import api from '../../services/api';

import Translation from '../../utils/getTranslation';

import ImageViewer from '../ImageViewer';

import {
  ContainerNotPermission,
  NotPermissionMessage,
  ButtonAuthorize,
  ButtonAuthorizeText,
  Container,
  Header,
  ButtonGoBack,
  IconGoBack,
  Footer,
  ButtonIcon,
  Icon,
  Picture,
} from './styles';

interface Camera {
  postId: number;
  userId: number;
  onSave: (id: number, photo: string) => void;
  onClose: () => void;
}

const Camera: React.FC<Camera> = ({ postId, userId, onSave, onClose }) => {
  const cameraRef = useRef<RNCamera | null>(null);

  const [hasPermission, setHasPermission] = useState(false);

  const [type, setType] = useState(RNCamera.Constants.Type.back);
  const [flash, setflash] = useState(RNCamera.Constants.FlashMode.off);

  const [loading, setLoading] = useState(false);

  const [photo, setPhoto] = useState<string | undefined>('');

  const takePicture = useCallback(async () => {
    if (cameraRef) {
      const options = {
        quality: 0.5,
        base64: false,
        exif: true,
      };

      const photoPicture = await cameraRef?.current?.takePictureAsync(options);

      if (photoPicture) {
        const photoPictureRotate = await ImageManipulator.manipulateAsync(
          photoPicture.uri,
          [
            {
              rotate: 0,
            },
            {
              resize: {
                width: photoPicture.width,
                height: photoPicture.height,
              },
            },
          ],
          {
            compress: 1,
          },
        );

        setPhoto(photoPictureRotate.uri);
      }
    }
  }, []);

  const handleSendPhoto = async () => {
    if (photo) {
      setLoading(true);

      try {
        const formData = new FormData();

        formData.append('type', 'SET_COMMENT');
        formData.append('id', String(postId));
        formData.append('user_id', String(userId));

        formData.append('file', {
          uri: photo,
          type: 'image/jpg',
          name: 'comment-image.jpg',
        });

        const response = await api.post('', formData);

        if (response.data.code !== 1) {
          throw new Error();
        }

        await onSave(response.data.id, photo);

        setLoading(false);
        setPhoto('');
        onClose();
      } catch (err) {
        setLoading(false);

        Alert.alert(
          Translation.t('camera.validation.titleError'),
          Translation.t('camera.validation.subtitleError'),
        );
      }
    }
  };

  const getHasPermission = useCallback(async () => {
    const { status } = await RNCamera.requestPermissionsAsync();

    setHasPermission(status === 'granted');
  }, []);

  useEffect(() => {
    getHasPermission();
  }, []);

  if (!hasPermission) {
    return (
      <ContainerNotPermission>
        <Header>
          <ButtonGoBack onPress={onClose}>
            <IconGoBack name="chevron-down" size={18} />
          </ButtonGoBack>
        </Header>

        <NotPermissionMessage>
          {Translation.t('camera.permission.message')}
        </NotPermissionMessage>

        <ButtonAuthorize onPress={() => Linking.openSettings()}>
          <ButtonAuthorizeText>
            {Translation.t('camera.permission.button')}
          </ButtonAuthorizeText>
        </ButtonAuthorize>
      </ContainerNotPermission>
    );
  }

  return (
    <Container>
      {photo ? (
        <ImageViewer
          photo={photo}
          loading={loading}
          onClose={() => setPhoto('')}
          onSave={handleSendPhoto}
        />
      ) : (
        <RNCamera
          ref={cameraRef}
          type={type}
          flashMode={flash}
          style={{ flex: 1 }}
        >
          <Header>
            <ButtonGoBack onPress={onClose}>
              <IconGoBack name="chevron-down" size={18} />
            </ButtonGoBack>
          </Header>

          <Footer>
            <ButtonIcon
              onPress={() =>
                setflash(
                  flash === RNCamera.Constants.FlashMode.off
                    ? RNCamera.Constants.FlashMode.on
                    : RNCamera.Constants.FlashMode.off,
                )
              }
            >
              <Icon
                name={
                  flash === RNCamera.Constants.FlashMode.off ? 'zap-off' : 'zap'
                }
                size={22}
              />
            </ButtonIcon>

            <Picture onPress={takePicture} />

            <ButtonIcon
              onPress={() =>
                setType(
                  type === RNCamera.Constants.Type.back
                    ? RNCamera.Constants.Type.front
                    : RNCamera.Constants.Type.back,
                )
              }
            >
              <Icon name="refresh-ccw" size={22} />
            </ButtonIcon>
          </Footer>
        </RNCamera>
      )}
    </Container>
  );
};

export default Camera;
