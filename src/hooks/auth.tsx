import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useEffect,
} from 'react';
import { Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import api from '../services/api';

import Translation from '../utils/getTranslation';

interface User {
  id: number;
  name: string;
  registration: number;
  email: string;
  phone: string;
  avatar_url: string;
  zone: string;
  role: string;
}

interface AuthState {
  user: User;
}

export interface SignInCredentials {
  registration: string;
  password: string;
}

export interface UpdateUserFields {
  name: string;
  email: string;
  password?: string;
  newPassword?: string;
}

interface AuthContextData {
  loadingViews: boolean;
  user: User;
  loading: boolean;
  signIn(credentials: SignInCredentials): Promise<void>;
  signOut(): void;
  updateUser(data: UpdateUserFields): Promise<boolean>;
  updateUserPhoto(photo: string): Promise<void>;
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

export const AuthProvider: React.FC = ({ children }) => {
  const [loadingViews, setLoadingViews] = useState(true);

  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<AuthState>({} as AuthState);

  const loadStorageData = useCallback(async (): Promise<void> => {
    const user = await AsyncStorage.getItem('@InstaSEG:user');

    if (user) {
      setData({
        user: JSON.parse(user),
      });
    }

    setLoadingViews(false);
  }, []);

  const signIn = useCallback(
    async ({ registration, password }: SignInCredentials) => {
      setLoading(true);

      try {
        const response = await api.post('', {
          type: 'AUTHENTICATION',
          registration,
          password,
        });

        const { user } = response.data;

        await AsyncStorage.setItem('@InstaSEG:user', JSON.stringify(user));

        setLoading(false);
        setData({ user });
      } catch (err) {
        setLoading(false);

        Alert.alert(
          Translation.t('auth.signIn.validation.titleError'),
          Translation.t('auth.signIn.validation.subtitleError'),
        );
      }
    },
    [],
  );

  const signOut = useCallback(async () => {
    await AsyncStorage.removeItem('@InstaSEG:user');

    setData({} as AuthState);
  }, []);

  const updateUser = async ({
    name,
    email,
    password,
    newPassword,
  }: UpdateUserFields): Promise<boolean> => {
    try {
      const response = await api.post('', {
        type: 'UPDATE_USER',
        id: data.user.id,
        name,
        email,
        password,
        newPassword,
      });

      if (response.data.code === 0) {
        throw new Error(Translation.t('auth.updateUser.validation.userError'));
      }

      if (response.data.code === 2) {
        throw new Error(
          Translation.t('auth.updateUser.validation.passwordError'),
        );
      }

      const newUserData = {
        ...data.user,
        name,
        email,
      };

      await AsyncStorage.setItem('@InstaSEG:user', JSON.stringify(newUserData));

      setData({ user: newUserData });

      return true;
    } catch (err) {
      Alert.alert(
        Translation.t('auth.updateUser.validation.titleError'),
        err.message,
      );

      return false;
    }
  };

  const updateUserPhoto = async (photo: string) => {
    try {
      const formData = new FormData();

      formData.append('type', 'SET_USER_PHOTO');
      formData.append('id', String(data.user.id));

      formData.append('file', {
        uri: photo,
        type: 'image/jpg',
        name: 'avatar-image.jpg',
      });

      const response = await api.post('', formData);

      if (response.data.code !== 1) {
        throw new Error();
      }

      const newUserData = {
        ...data.user,
        avatar_url: response.data.avatar_url,
      };

      await AsyncStorage.setItem('@InstaSEG:user', JSON.stringify(newUserData));

      setData({ user: newUserData });
    } catch (err) {
      Alert.alert(
        Translation.t('auth.updateUserPhoto.validation.titleError'),
        Translation.t('auth.updateUserPhoto.validation.subtitleError'),
      );
    }
  };

  useEffect(() => {
    loadStorageData();
  }, []);

  return (
    <AuthContext.Provider
      value={{
        loadingViews,
        user: data.user,
        signIn,
        signOut,
        updateUser,
        updateUserPhoto,
        loading,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth(): AuthContextData {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error('useAuth must be used within an AuthProvider');
  }

  return context;
}
