import React from 'react';
import { FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { BorderlessButton } from 'react-native-gesture-handler';

import {
  Container,
  Header,
  Icon,
  Notification,
  Information,
  Avatar,
  Description,
  Name,
  Timer,
  Photo,
} from './styles';

const Notifications: React.FC = () => {
  const navigation = useNavigation();

  return (
    <Container>
      <Header>
        <BorderlessButton onPress={() => navigation.goBack()}>
          <Icon name="arrow-left" size={24} />
        </BorderlessButton>
      </Header>

      <FlatList
        data={[
          {
            id: 1,
            user: {
              id: 1,
              name: 'Daniel Avila',
              avatar_url:
                'https://avatars1.githubusercontent.com/u/64021671?s=460&u=9ef60e18bf6712ad4bed953078ba10ff4b99d957&v=4',
            },
            created_at: '05/10/2020 11:42',
            image_url:
              'https://avatars1.githubusercontent.com/u/64021671?s=460&u=9ef60e18bf6712ad4bed953078ba10ff4b99d957&v=4',
          },
        ]}
        keyExtractor={(item) => String(item.id)}
        renderItem={({ item }) => (
          <Notification>
            <Avatar source={{ uri: item.user.avatar_url }} />

            <Information>
              <Description>
                <Name>{item.user.name} </Name>
                mencionou você em um comentário
              </Description>

              <Timer>{item.created_at}</Timer>
            </Information>

            <Photo source={{ uri: item.image_url }} />
          </Notification>
        )}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          paddingBottom: 32,
        }}
      />
    </Container>
  );
};

export default Notifications;
