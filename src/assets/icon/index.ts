import IconLogo from './IconLogo';
import IconFlagBrazil from './IconFlagBrazil';
import IconFlagSpain from './IconFlagSpain';
import IconFlagEua from './IconFlagEua';

export { IconLogo, IconFlagBrazil, IconFlagSpain, IconFlagEua };
