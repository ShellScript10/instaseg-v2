import React, { useCallback, useEffect, useRef, useState } from 'react';
import {
  Alert,
  FlatList,
  KeyboardAvoidingView,
  Modal,
  Platform,
} from 'react-native';
import { useRoute, useNavigation } from '@react-navigation/native';

import api from '../../services/api';

import { useAuth } from '../../hooks/auth';

import Translation from '../../utils/getTranslation';
import getDateNow from '../../utils/getDateNow';

import Post, { PostItem } from '../../components/Post';
import Comment, { CommentItem } from '../../components/Comment';
import InputDefault from '../../components/Input/Default';
import Camera from '../../components/Camera';

import {
  Container,
  Header,
  ButtonGoBack,
  IconGoBack,
  Footer,
  ButtonCamera,
  Icon,
} from './styles';

interface PostProps {
  post: PostItem;
}

const Comments: React.FC = () => {
  const navigation = useNavigation();

  const { params } = useRoute();

  const { post: postProp } = params as PostProps;

  const { user } = useAuth();

  const scrollRef = useRef<FlatList>(null);

  const [modalVisible, setModalVisible] = useState(false);

  const [comment, setComment] = useState('');

  const [post, setPost] = useState<PostItem>(postProp);
  const [comments, setComments] = useState<CommentItem[]>([]);

  const getComments = useCallback(async () => {
    const response = await api.post('', {
      type: 'GET_COMMENTS',
      id: post.id,
    });

    setComments(response.data);
  }, []);

  const handleUpdateQuantityComments = () => {
    const newComments = {
      ...post,
      // eslint-disable-next-line radix
      comments: String(parseInt(post.comments) + 1),
    };

    setPost(newComments);
  };

  const handleComment = async () => {
    if (comment !== '') {
      try {
        const formData = new FormData();

        formData.append('type', 'SET_COMMENT');
        formData.append('id', String(post.id));
        formData.append('user_id', String(user.id));
        formData.append('description', comment);

        const response = await api.post('', formData);

        if (response.data.code !== 1) {
          throw new Error();
        }

        setComments((state: CommentItem[]) => [
          ...state,
          {
            id: response.data.id,
            description: comment,
            photo: null,
            owner: {
              name: user.name,
              avatar_url: user.avatar_url,
            },
            created_at: getDateNow(),
          },
        ]);

        handleUpdateQuantityComments();

        setComment('');
      } catch (err) {
        Alert.alert(
          Translation.t('comments.validation.titleError'),
          Translation.t('comments.validation.subtitleError'),
        );
      }
    }
  };

  const handleCommentPhoto = async (id: number, photo: string) => {
    setComments((state: CommentItem[]) => [
      ...state,
      {
        id,
        description: '',
        photo,
        owner: {
          name: user.name,
          avatar_url: user.avatar_url,
        },
        created_at: getDateNow(),
      },
    ]);

    handleUpdateQuantityComments();
  };

  const handleScrollToEnd = () => {
    scrollRef.current?.scrollToEnd({ animated: true });
  };

  useEffect(() => {
    getComments();
  }, []);

  return (
    <Container>
      <Header>
        <ButtonGoBack onPress={() => navigation.goBack()}>
          <IconGoBack name="chevron-left" size={18} />
        </ButtonGoBack>
      </Header>

      <FlatList<CommentItem>
        ref={scrollRef}
        data={comments}
        keyExtractor={(item) => String(item.id)}
        renderItem={({ item }) => <Comment data={item} />}
        ListHeaderComponent={<Post data={post} details />}
        showsVerticalScrollIndicator={false}
        onContentSizeChange={() => comments.length > 0 && handleScrollToEnd()}
      />

      <KeyboardAvoidingView behavior="padding" enabled={Platform.OS === 'ios'}>
        <Footer>
          <ButtonCamera onPress={() => setModalVisible(true)}>
            <Icon name="camera" size={22} />
          </ButtonCamera>

          <InputDefault
            autoCorrect
            keyboardType="default"
            placeholder={Translation.t('comments.inputPlaceholder')}
            returnKeyType="send"
            value={comment}
            onChangeText={setComment}
            onSubmitEditing={handleComment}
          />
        </Footer>
      </KeyboardAvoidingView>

      <Modal animationType="fade" visible={modalVisible}>
        <Camera
          postId={post.id}
          userId={user.id}
          onSave={handleCommentPhoto}
          onClose={() => setModalVisible(false)}
        />
      </Modal>
    </Container>
  );
};

export default Comments;
