import React, { useCallback, useContext, useRef, useState } from 'react';
import { Platform, ActivityIndicator, Alert, TextInput } from 'react-native';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/mobile';
import * as Yup from 'yup';
// import { BorderlessButton } from 'react-native-gesture-handler';
import { ThemeContext } from 'styled-components';

import { SignInCredentials, useAuth } from '../../hooks/auth';

import Translation from '../../utils/getTranslation';
import getValidationErrors from '../../utils/getValidationErrors';

import Input from '../../components/Input';

import { IconLogo } from '../../assets/icon';

import {
  Container,
  Header,
  Main,
  Title,
  // ButtonForgotPasswordText,
  ButtonLogin,
  ButtonLoginText,
} from './styles';

const SignIn: React.FC = () => {
  const { signIn, loading } = useAuth();

  const { colors } = useContext(ThemeContext);

  const formRef = useRef<FormHandles>(null);
  const passwordInputRef = useRef<TextInput>(null);

  const [isInputSecureText, setIsInputSecureText] = useState(true);

  const handleSecureTextEntry = () => {
    setIsInputSecureText(!isInputSecureText);
  };

  const handleSignIn = useCallback(async (data: SignInCredentials) => {
    try {
      formRef.current?.setErrors({});

      const schema = Yup.object().shape({
        registration: Yup.number().required(
          Translation.t('signIn.validation.registration'),
        ),
        password: Yup.string().required(
          Translation.t('signIn.validation.password'),
        ),
      });

      await schema.validate(data, {
        abortEarly: false,
      });

      await signIn(data);
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errors = getValidationErrors(err);

        formRef.current?.setErrors(errors);
      }

      Alert.alert(
        Translation.t('signIn.validation.titleError'),
        Translation.t('signIn.validation.subtitleError'),
      );
    }
  }, []);

  return (
    <Container behavior="padding" enabled={Platform.OS === 'ios'}>
      <Header>
        <IconLogo width={168} />
      </Header>

      <Main>
        <Title>{Translation.t('signIn.title')}</Title>

        <Form ref={formRef} onSubmit={handleSignIn}>
          <Input
            name="registration"
            icon="tag"
            placeholder={Translation.t('signIn.form.registration')}
            autoCorrect={false}
            autoCapitalize="none"
            keyboardType="number-pad"
            returnKeyType="next"
            borderRadiusTop
            onSubmitEditing={() => passwordInputRef.current?.focus()}
          />

          <Input
            ref={passwordInputRef}
            name="password"
            icon="lock"
            placeholder={Translation.t('signIn.form.password')}
            iconRight={isInputSecureText ? 'eye' : 'eye-off'}
            onClickIconRight={handleSecureTextEntry}
            returnKeyType="send"
            secureTextEntry={isInputSecureText}
            borderRadiusBottom
            onSubmitEditing={() => formRef.current?.submitForm()}
          />
        </Form>

        <ButtonLogin
          onPress={() => formRef.current?.submitForm()}
          enabled={!loading}
        >
          {!loading ? (
            <ButtonLoginText>
              {Translation.t('signIn.form.button')}
            </ButtonLoginText>
          ) : (
            <ActivityIndicator color={colors.white} size="small" />
          )}
        </ButtonLogin>

        {/* <BorderlessButton style={{ alignSelf: 'center' }}>
          <ButtonForgotPasswordText>
            {Translation.t('signIn.form.forgotPassword')}
          </ButtonForgotPasswordText>
        </BorderlessButton> */}
      </Main>
    </Container>
  );
};

export default SignIn;
