import React, { useContext } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { ThemeContext } from 'styled-components';
import { Feather } from '@expo/vector-icons';

import Feed from '../pages/Feed';
import Search from '../pages/Search';
import User from '../pages/User';

const Tabs = createBottomTabNavigator();

const TabsRoutes: React.FC = () => {
  const { colors } = useContext(ThemeContext);

  return (
    <Tabs.Navigator
      initialRouteName="Feed"
      tabBarOptions={{
        showLabel: false,
        activeTintColor: colors.primary,
        inactiveTintColor: colors.additional,
      }}
    >
      <Tabs.Screen
        name="Feed"
        component={Feed}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Feather name="home" color={color} size={size} />
          ),
        }}
      />

      <Tabs.Screen
        name="Search"
        component={Search}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Feather name="search" color={color} size={size} />
          ),
        }}
      />

      <Tabs.Screen
        name="User"
        component={User}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Feather name="user" color={color} size={size} />
          ),
        }}
      />
    </Tabs.Navigator>
  );
};

export default TabsRoutes;
