import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';
import { Feather } from '@expo/vector-icons';

export const Container = styled.KeyboardAvoidingView`
  background: ${(props) => props.theme.colors.background};

  flex: 1;
`;

export const LoadingContainer = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.4);
  z-index: 2;

  align-items: center;
  justify-content: center;
`;

export const Header = styled.View`
  height: 168px;
  background: ${(props) => props.theme.colors.primary};
  padding: 0 24px;

  justify-content: center;
`;

export const Icon = styled(Feather)`
  color: ${(props) => props.theme.colors.white};
`;

export const User = styled.View`
  margin-top: -60px;

  align-items: center;
`;

export const Avatar = styled.Image`
  width: 120px;
  height: 120px;
  background: ${(props) => props.theme.colors.additional};
  border-width: 4px;
  border-color: ${(props) => props.theme.colors.background};
  border-radius: 60px;
`;

export const Description = styled.Text`
  font-family: Lato_400Regular;
  font-size: 12px;
  color: ${(props) => props.theme.colors.additional};
  margin: 3px 0 32px;
`;

export const Separator = styled.View`
  height: 1px;
  background: ${(props) => props.theme.colors.border};
  margin-bottom: 16px;
`;

export const Button = styled(RectButton)`
  height: 56px;
  background: ${(props) => props.theme.colors.secondary};
  border-radius: 8px;
  margin-top: 16px;

  align-items: center;
  justify-content: center;
`;

export const ButtonText = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 16px;
  color: ${(props) => props.theme.colors.white};
`;
