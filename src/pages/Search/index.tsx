import React, { useContext, useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, RefreshControl } from 'react-native';
import { BorderlessButton } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { ThemeContext } from 'styled-components';

import api from '../../services/api';

import Translation from '../../utils/getTranslation';

import shuffleArray from '../../utils/shuffleArray';

import { PostItem } from '../../components/Post';

import Input from '../../components/Input/Default';

import { Container, Header, Photo, EmptyMessage } from './styles';

export interface PhotoItem {
  id: number;
  photo_url: string;
  post: PostItem;
}

const Search: React.FC = () => {
  const navigation = useNavigation();

  const { colors } = useContext(ThemeContext);

  const [page, setPage] = useState(1);
  const [refreshing, setRefreshing] = useState(false);
  const [refreshingScroll, setRefreshingScroll] = useState(false);
  const [more, setMore] = useState(false);

  const [search, setSearch] = useState('');
  const [photos, setPhotos] = useState<PhotoItem[]>([]);

  const getPhotos = async () => {
    setRefreshing(true);
    setPage(2);
    setMore(true);

    const response = await api.post('', {
      type: 'GET_IMAGES',
      search,
      page: 1,
    });

    setPhotos(shuffleArray(response.data));

    setRefreshing(false);
  };

  const getPhotosScroll = async () => {
    if (refreshingScroll) return;

    setRefreshingScroll(true);

    const response = await api.post('', {
      type: 'GET_IMAGES',
      search,
      page,
    });

    if (response.data.length > 0) {
      setPage(page + 1);

      setPhotos((state) => {
        if (photos.length === 0) {
          return shuffleArray(response.data);
        }

        return [...state, ...shuffleArray(response.data)];
      });
    } else {
      setMore(false);
    }

    setRefreshingScroll(false);
  };

  const handleSearchPhotos = (text: string) => {
    setSearch(text);

    getPhotos();
  };

  useEffect(() => {
    getPhotos();
  }, []);

  return (
    <Container>
      <Header>
        <Input
          showLeftIcon
          placeholder={Translation.t('search.inputPlaceholder')}
          value={search}
          onChangeText={handleSearchPhotos}
        />
      </Header>

      <FlatList
        data={photos}
        keyExtractor={(item) => String(item.id)}
        numColumns={3}
        renderItem={({ item }) => (
          <BorderlessButton
            onPress={() => navigation.navigate('Comments', { post: item.post })}
          >
            <Photo source={{ uri: item.photo_url }} />
          </BorderlessButton>
        )}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={getPhotos} />
        }
        onEndReached={() => more && getPhotosScroll()}
        onEndReachedThreshold={0.1}
        ListFooterComponent={
          refreshingScroll ? (
            <ActivityIndicator
              color={colors.primary}
              size="small"
              style={{ marginTop: 16 }}
            />
          ) : null
        }
        ListEmptyComponent={
          <EmptyMessage>{Translation.t('search.emptyMessage')}</EmptyMessage>
        }
      />
    </Container>
  );
};

export default Search;
