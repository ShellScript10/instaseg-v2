export default {
  title: 'light',
  colors: {
    primary: '#A32427',
    secondary: '#F5BD44',
    background: '#fafafa',
    card: '#ffffff',
    border: '#D5D5D5',
    additional: '#D5D5D5',
    title: '#454545',
    text: '#909090',
    white: '#ffffff',
    alert: '#EB474B',
    success: '#0AC228',
    info: '#203E95',
  },
};
