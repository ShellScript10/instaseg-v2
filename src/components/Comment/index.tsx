import React, { useState } from 'react';
import { Modal } from 'react-native';
import ImageViewer from '../ImageViewer';

import {
  Container,
  Avatar,
  Informations,
  Description,
  Name,
  ButtonPhoto,
  Photo,
  Time,
} from './styles';

export interface CommentItem {
  id: number;
  description: string | null;
  photo: string | null;
  owner: {
    name: string;
    avatar_url: string;
  };
  created_at: string;
}

interface CommentProps {
  data: CommentItem;
}

const Comment: React.FC<CommentProps> = ({ data }) => {
  const [modalVisible, setModalVisible] = useState(false);

  return (
    <Container>
      <Avatar source={{ uri: data.owner.avatar_url }} />

      <Informations>
        <Description>
          <Name>{data.owner.name}</Name>
          {` ${data.description && data.description}`}
        </Description>

        {data.photo && (
          <ButtonPhoto onPress={() => setModalVisible(true)}>
            <Photo source={{ uri: data.photo }} />
          </ButtonPhoto>
        )}

        <Time>{data.created_at}</Time>
      </Informations>

      {data.photo && (
        <Modal animationType="fade" visible={modalVisible}>
          <ImageViewer
            photo={data.photo}
            onClose={() => setModalVisible(false)}
          />
        </Modal>
      )}
    </Container>
  );
};

export default Comment;
