import React from 'react';
import { View } from 'react-native';

import { useAuth } from '../hooks/auth';

import AuthRoutes from './auth.routes';
import AppRoutes from './app.routes';

const Routes: React.FC = () => {
  const { user, loadingViews } = useAuth();

  if (loadingViews) {
    return <View />;
  }

  return user ? <AppRoutes /> : <AuthRoutes />;
};

export default Routes;
