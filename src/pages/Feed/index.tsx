import React, { useContext, useCallback, useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, RefreshControl } from 'react-native';
import { ThemeContext } from 'styled-components';

import api from '../../services/api';

import { useAuth } from '../../hooks/auth';

import Translation from '../../utils/getTranslation';

import Post, { PostItem } from '../../components/Post';

import { IconLogo } from '../../assets/icon';

import { Container, Header, EmptyMessage } from './styles';

const Feed: React.FC = () => {
  const { colors } = useContext(ThemeContext);

  const { user } = useAuth();

  const [page, setPage] = useState(1);
  const [refreshing, setRefreshing] = useState(false);
  const [refreshingScroll, setRefreshingScroll] = useState(false);
  const [more, setMore] = useState(true);

  const [posts, setPosts] = useState<PostItem[]>([]);

  const getPosts = async () => {
    setRefreshing(true);
    setPage(2);
    setMore(true);

    const response = await api.post('', {
      type: 'GET_POSTS',
      page: 1,
    });

    if (response.data.length > 0) {
      setPosts(response.data);
    }

    setRefreshing(false);
  };

  const getPostsScroll = async () => {
    if (refreshingScroll) return;

    setRefreshingScroll(true);

    const response = await api.post('', {
      type: 'GET_POSTS',
      page,
    });

    if (response.data.length > 0) {
      setPage(page + 1);

      setPosts((prevPosts) => {
        if (posts.length === 0) {
          return response.data;
        }

        return [...prevPosts, ...response.data];
      });
    } else {
      setMore(false);
    }

    setRefreshingScroll(false);
  };

  const handleSetViewed = useCallback(async (data: PostItem) => {
    await api.post('', {
      type: 'SET_VIEW',
      id: data.id,
      user_id: user.id,
    });
  }, []);

  useEffect(() => {
    getPosts();
  }, []);

  return (
    <Container>
      <Header>
        <IconLogo width={80} />
      </Header>

      <FlatList<PostItem>
        data={posts}
        keyExtractor={(item) => String(item.id)}
        renderItem={({ item }) => (
          <Post data={item} onNavigate={handleSetViewed} />
        )}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          paddingTop: 16,
        }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={getPosts} />
        }
        onEndReached={() => more && getPostsScroll()}
        onEndReachedThreshold={0.1}
        ListFooterComponent={
          refreshingScroll ? (
            <ActivityIndicator color={colors.primary} size="small" />
          ) : null
        }
        ListEmptyComponent={
          <EmptyMessage>{Translation.t('feed.emptyMessage')}</EmptyMessage>
        }
      />
    </Container>
  );
};
export default Feed;
