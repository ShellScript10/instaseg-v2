import * as Localization from 'expo-localization';
import i18n from 'i18n-js';

import { en, es, pt } from '../config/locales';

i18n.translations = {
  en,
  pt,
  es,
};

i18n.locale = Localization.locale;

i18n.fallbacks = true;

export default i18n;
