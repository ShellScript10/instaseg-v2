export default {
  welcome: {
    title: 'Seja bem-vindo',
    subtitle: 'Escolha por onde deseja continuar com sua experiência',
    buttonLogin: 'Já tenha uma conta',
    buttonRegister: 'Criar conta',
  },
  signIn: {
    title: 'Fazer login',
    form: {
      registration: 'Matrícula',
      password: 'Senha',
      button: 'Entrar',
      forgotPassword: 'Esqueci minha senha?',
    },
    validation: {
      registration: 'Matrícula é obrigatório',
      password: 'Senha é obrigatória',
      titleError: 'Erro na autenticação',
      subtitleError: 'Ocorreu um erro ao fazer o login, cheque as credenciais',
    },
  },
  feed: {
    emptyMessage: `Nenhum registro encontrado ${'\n'} no momento 😢`,
  },
  search: {
    inputPlaceholder: 'Pesquisar...',
    emptyMessage: `Nenhum registro encontrado ${'\n'} no momento 😢`,
  },
  profile: {
    description: 'Clique para trocar sua foto',
    form: {
      name: 'Nome',
      registration: 'Matrícula',
      email: 'E-mail',
      oldPassword: 'Senha antiga',
      newPassword: 'Nova senha',
      button: 'Salvar informações',
    },
    validation: {
      name: 'Nome é obrigatório',
      email: 'E-mail é obrigatório',
      titleError: 'Erro na atualização dos dados',
      subtitleError: 'Ocorreu um erro ao fazer a atualização, cheque os campos',
    },
  },
  user: {
    buttons: {
      profile: 'Meu perfil',
      notification: 'Notificações',
      signOut: 'Sair',
    },
  },
  comments: {
    inputPlaceholder: 'Adicione um comentário',
    validation: {
      titleError: 'Erro',
      subtitleError: 'Ocorreu um problema ao cadastrar o seu comentário',
    },
  },
  camera: {
    permission: {
      message: `Permissão de acesso a câmera,${'\n'} não autorizada`,
      button: 'Autorizar',
    },
    validation: {
      titleError: 'Erro',
      subtitleError: 'Não foi possível cadastrar a sua foto',
    },
  },
  post: {
    postedBy: 'Postado por',
    toBeContinued: 'contínua',
  },
  auth: {
    signIn: {
      validation: {
        titleError: 'Erro na autenticação',
        subtitleError:
          'Ocorreu um erro ao fazer o login, cheque as credenciais',
      },
    },
    updateUser: {
      validation: {
        titleError: 'Erro na atualização',
        userError: 'Usuário não pode ser editado. Por favor, tente mais tarde',
        passwordError: 'Senha informada não confere com a registrada',
      },
    },
    updateUserPhoto: {
      validation: {
        titleError: 'Erro',
        subtitleError: 'Não foi possível atualizar a sua foto de perfil',
      },
    },
  },
};
