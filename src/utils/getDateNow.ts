import { parseISO, format, formatRelative, formatDistance } from 'date-fns';

const getDateNow = (): string => format(new Date(), 'dd/MM/yyyy HH:mm');

export default getDateNow;
