import styled, { css } from 'styled-components/native';
import { Feather } from '@expo/vector-icons';
import { Platform } from 'react-native';

interface LoadingProps {
  loading?: boolean;
}

export const Container = styled.View`
  background: #000;

  flex: 1;
`;

export const Photo = styled.Image`
  flex: 1;
`;

export const Header = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  height: ${Platform.OS === 'ios' ? 104 : 80}px;
  background: rgba(0, 0, 0, 0.1);
  padding: 0 24px;

  ${Platform.OS === 'ios' &&
  css`
    padding: 32px 24px 0;
  `}

  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const ButtonGoBack = styled.TouchableOpacity`
  width: 32px;
  height: 32px;
  background: ${(props) => props.theme.colors.background};
  border-radius: 8px;

  align-items: center;
  justify-content: center;
`;

export const IconGoBack = styled(Feather)`
  color: ${(props) => props.theme.colors.primary};
`;

export const ButtonAprove = styled.TouchableOpacity<LoadingProps>`
  width: 32px;
  height: 32px;
  background: ${(props) => props.theme.colors.secondary};
  border-radius: 8px;

  align-items: center;
  justify-content: center;
`;

export const Icon = styled(Feather)`
  color: ${(props) => props.theme.colors.white};
`;
