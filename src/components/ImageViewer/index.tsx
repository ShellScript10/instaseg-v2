import React, { useContext } from 'react';
import { ActivityIndicator } from 'react-native';
import { ThemeContext } from 'styled-components';

import {
  Container,
  Photo,
  Header,
  ButtonGoBack,
  IconGoBack,
  ButtonAprove,
  Icon,
} from './styles';

interface ImageViewerProps {
  photo: string;
  loading?: boolean;
  onClose: () => void;
  onSave?: () => void;
}

const ImageViewer: React.FC<ImageViewerProps> = ({
  photo,
  loading,
  onClose,
  onSave,
}) => {
  const { colors } = useContext(ThemeContext);

  return (
    <Container>
      <Photo source={{ uri: photo }} />

      <Header>
        <ButtonGoBack onPress={() => !loading && onClose()}>
          <IconGoBack name="x" size={18} />
        </ButtonGoBack>

        {onSave && (
          <ButtonAprove onPress={() => !loading && onSave()} loading={loading}>
            {loading ? (
              <ActivityIndicator color={colors.white} size="small" />
            ) : (
              <Icon name="check" size={18} />
            )}
          </ButtonAprove>
        )}
      </Header>
    </Container>
  );
};

export default ImageViewer;
