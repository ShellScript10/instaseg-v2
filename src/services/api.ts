import axios from 'axios';

const api = axios.create({
  baseURL: __DEV__
    ? 'http://192.168.0.18:8888/ambev.clae.com.br/METODOS/am_metodos_api.php'
    : 'https://www.clae.com.br/app_ambev//METODOS/am_metodos_api.php',
});

export default api;
