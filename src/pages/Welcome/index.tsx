import React from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import Translation from '../../utils/getTranslation';

import {
  IconLogo,
  IconFlagBrazil,
  IconFlagSpain,
  IconFlagEua,
} from '../../assets/icon';

import {
  Container,
  Header,
  Flags,
  Main,
  Title,
  SubTitle,
  Buttons,
  Button,
  ButtonText,
  ButtonRegister,
  ButtonRegisterText,
} from './styles';

const Welcome: React.FC = () => {
  const navigation = useNavigation();

  return (
    <Container>
      <Header>
        <IconLogo width={168} />
      </Header>

      <Main>
        <Flags>
          <IconFlagBrazil width={40} style={{ marginRight: 16 }} />
          <IconFlagSpain width={40} style={{ marginRight: 16 }} />
          <IconFlagEua width={40} />
        </Flags>

        <View>
          <Title>{Translation.t('welcome.title')}</Title>

          <SubTitle>{Translation.t('welcome.subtitle')}</SubTitle>
        </View>

        <Buttons>
          <Button onPress={() => navigation.navigate('SignIn')}>
            <ButtonText>{Translation.t('welcome.buttonLogin')}</ButtonText>
          </Button>

          {/* <ButtonRegister onPress={() => navigation.navigate('SignUp')}>
            <ButtonRegisterText>
              {Translation.t('welcome.buttonRegister')}
            </ButtonRegisterText>
          </ButtonRegister> */}
        </Buttons>
      </Main>
    </Container>
  );
};

export default Welcome;
