import React, { useCallback, useContext, useRef, useState } from 'react';
import {
  ScrollView,
  Alert,
  ActivityIndicator,
  TextInput,
  Platform,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { BorderlessButton } from 'react-native-gesture-handler';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/mobile';
import * as ImagePicker from 'expo-image-picker';
import * as Yup from 'yup';
import { ThemeContext } from 'styled-components';

import { useAuth, UpdateUserFields } from '../../hooks/auth';

import Translation from '../../utils/getTranslation';
import getValidationErrors from '../../utils/getValidationErrors';

import Input from '../../components/Input';

import {
  Container,
  LoadingContainer,
  Header,
  Icon,
  User,
  Avatar,
  Description,
  Separator,
  Button,
  ButtonText,
} from './styles';

const Profile: React.FC = () => {
  const navigation = useNavigation();

  const { colors } = useContext(ThemeContext);

  const { user, updateUser, updateUserPhoto } = useAuth();

  const formRef = useRef<FormHandles>(null);

  const emailRef = useRef<TextInput>(null);
  const passwordInputRef = useRef<TextInput>(null);

  const [loading, setLoading] = useState(false);

  const handleUpdateUser = useCallback(async (data: UpdateUserFields) => {
    try {
      formRef.current?.setErrors({});

      const schema = Yup.object().shape({
        name: Yup.string().required(Translation.t('profile.validation.name')),
        email: Yup.string().required(Translation.t('profile.validation.email')),
        password: Yup.string(),
        newPassword: Yup.string().when(
          'password',
          (password: string, field: any) => {
            if (password) {
              return field.required();
            }

            return field;
          },
        ),
      });

      await schema.validate(data, {
        abortEarly: false,
      });

      const validateUpdate = await updateUser(data);

      if (validateUpdate) {
        navigation.goBack();
      }
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errors = getValidationErrors(err);

        formRef.current?.setErrors(errors);
      }

      Alert.alert(
        Translation.t('profile.validation.titleError'),
        Translation.t('profile.validation.subtitleError'),
      );
    }
  }, []);

  const pickerImagePermission = useCallback(async () => {
    const { status } = await ImagePicker.requestCameraRollPermissionsAsync();

    if (status !== 'granted') {
      Alert.alert('Alerta', 'Você não possui permissão');
    }
  }, []);

  const pickImage = useCallback(async () => {
    pickerImagePermission();

    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      quality: 0,
    });

    if (!result.cancelled) {
      setLoading(true);

      if (result.uri) {
        await updateUserPhoto(result.uri);
      }

      setLoading(false);
    }
  }, []);

  return (
    <Container behavior="padding" enabled={Platform.OS === 'ios'}>
      {loading && (
        <LoadingContainer>
          <ActivityIndicator color={colors.primary} size="small" />
        </LoadingContainer>
      )}

      <Header>
        <BorderlessButton onPress={() => navigation.goBack()}>
          <Icon name="arrow-left" size={24} />
        </BorderlessButton>
      </Header>

      <User>
        <BorderlessButton onPress={pickImage}>
          <Avatar
            source={{
              uri: user.avatar_url,
            }}
          />
        </BorderlessButton>

        <Description>{Translation.t('profile.description')}</Description>
      </User>

      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: 32 }}
      >
        <Form
          ref={formRef}
          onSubmit={handleUpdateUser}
          style={{ paddingHorizontal: 24 }}
        >
          <Input
            name="name"
            icon="user"
            placeholder={Translation.t('profile.form.name')}
            returnKeyType="next"
            defaultValueInput={user.name}
            onSubmitEditing={() => emailRef.current?.focus()}
          />

          <Input
            name="registration"
            icon="tag"
            placeholder={Translation.t('profile.form.registration')}
            defaultValueInput={String(user.registration)}
            editable={false}
          />

          <Input
            ref={emailRef}
            name="email"
            icon="mail"
            placeholder={Translation.t('profile.form.email')}
            returnKeyType="send"
            defaultValueInput={user.email}
            onSubmitEditing={() => formRef.current?.submitForm()}
          />

          <Separator />

          <Input
            name="password"
            icon="lock"
            placeholder={Translation.t('profile.form.oldPassword')}
            returnKeyType="next"
            secureTextEntry
            onSubmitEditing={() => passwordInputRef.current?.focus()}
          />

          <Input
            ref={passwordInputRef}
            name="newPassword"
            icon="lock"
            placeholder={Translation.t('profile.form.newPassword')}
            returnKeyType="send"
            secureTextEntry
            onSubmitEditing={() => formRef.current?.submitForm()}
          />

          <Button onPress={() => formRef.current?.submitForm()}>
            <ButtonText>{Translation.t('profile.form.button')}</ButtonText>
          </Button>
        </Form>
      </ScrollView>
    </Container>
  );
};

export default Profile;
