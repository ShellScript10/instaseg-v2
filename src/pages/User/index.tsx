import React from 'react';
import { useNavigation } from '@react-navigation/native';

import { useAuth } from '../../hooks/auth';

import Translation from '../../utils/getTranslation';

import {
  Container,
  Header,
  UserInformations,
  Avatar,
  Name,
  Zone,
  Role,
  Actions,
  Button,
  Icon,
  Text,
  ButtonOutline,
  IconOutline,
  TextOutline,
} from './styles';

const User: React.FC = () => {
  const navigation = useNavigation();

  const { user, signOut } = useAuth();

  return (
    <Container>
      <Header />

      <UserInformations>
        <Avatar
          source={{
            uri: user.avatar_url,
          }}
        />

        <Name>{user.name}</Name>

        <Role>{user.role}</Role>

        <Zone>{user.zone}</Zone>
      </UserInformations>

      <Actions>
        <Button onPress={() => navigation.navigate('Profile')}>
          <Icon name="user" size={18} />

          <Text>{Translation.t('user.buttons.profile')}</Text>
        </Button>

        {/* <Button onPress={() => navigation.navigate('Notifications')}>
          <Icon name="bell" size={18} />

          <Text>{Translation.t('user.buttons.notification')}</Text>
        </Button> */}
      </Actions>

      <ButtonOutline onPress={signOut}>
        <IconOutline name="log-out" size={18} />

        <TextOutline>{Translation.t('user.buttons.signOut')}</TextOutline>
      </ButtonOutline>
    </Container>
  );
};

export default User;
