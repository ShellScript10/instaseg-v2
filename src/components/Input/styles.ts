import styled, { css } from 'styled-components/native';
import { Feather } from '@expo/vector-icons';
import { BorderlessButton } from 'react-native-gesture-handler';

interface ContainerProps {
  borderRadiusTop?: boolean;
  borderRadiusBottom?: boolean;
  editable?: boolean;
}

interface FocusProps {
  isFocused: boolean;
}

export const Container = styled.View<ContainerProps>`
  width: 100%;
  height: 56px;
  background: ${(props) => props.theme.colors.card};
  border-width: 1px;
  border-color: ${(props) => props.theme.colors.border};

  flex-direction: row;
  align-items: center;

  ${(props) =>
    !props.editable &&
    css`
      background: ${props.theme.colors.background};
    `}

  ${(props) =>
    props.borderRadiusTop &&
    css`
      border-top-left-radius: 8px;
      border-top-right-radius: 8px;
      border-bottom-width: 0.5px;
    `}

  ${(props) =>
    props.borderRadiusBottom &&
    css`
      border-bottom-left-radius: 8px;
      border-bottom-right-radius: 8px;
      border-top-width: 0.5px;
    `}

  ${(props) =>
    !props.borderRadiusTop &&
    !props.borderRadiusBottom &&
    css`
      border-radius: 8px;
      margin-bottom: 16px;
    `}
`;

export const Indicator = styled.View`
  width: 2px;
  height: 32px;
  background: ${(props) => props.theme.colors.primary};
  border-radius: 8px;
`;

export const Icon = styled(Feather)<FocusProps>`
  margin: 0 16px;
  color: ${(props) => props.theme.colors.additional};

  ${(props) =>
    props.isFocused &&
    css`
      color: ${props.theme.colors.primary};
    `}
`;

export const TextInput = styled.TextInput`
  height: 100%;
  font-family: Lato_400Regular;
  font-size: 14px;
  color: ${(props) => props.theme.colors.text};
  margin-right: 16px;

  flex: 1;
`;

export const ButtonRight = styled(BorderlessButton)`
  height: 100%;
  margin-right: 16px;

  align-items: center;
  justify-content: center;
`;

export const IconRight = styled(Feather)`
  color: ${(props) => props.theme.colors.secondary};
`;
