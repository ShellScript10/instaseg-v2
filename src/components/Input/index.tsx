import React, {
  useRef,
  useContext,
  useState,
  useCallback,
  useEffect,
  useImperativeHandle,
  forwardRef,
} from 'react';
import { TextInputProps } from 'react-native';
import { useField } from '@unform/core';
import { ThemeContext } from 'styled-components';

import {
  Container,
  Indicator,
  Icon,
  TextInput,
  ButtonRight,
  IconRight,
} from './styles';

interface InputProps extends TextInputProps {
  defaultValueInput?: string;
  name: string;
  icon: string;
  borderRadiusTop?: boolean;
  borderRadiusBottom?: boolean;
  editable?: boolean;
  iconRight?: string;
  onClickIconRight?: () => void;
}

interface InputValueReference {
  value: string;
}

interface InputRef {
  focus(): void;
}

const Input: React.ForwardRefRenderFunction<InputRef, InputProps> = (
  {
    defaultValueInput = '',
    name,
    icon,
    borderRadiusTop,
    borderRadiusBottom,
    editable = true,
    iconRight,
    onClickIconRight,
    ...rest
  },
  ref,
) => {
  const inputElementRef = useRef<any>(null);

  const { colors } = useContext(ThemeContext);

  const {
    fieldName,
    defaultValue = defaultValueInput,
    error,
    registerField,
  } = useField(name);

  const inputValueRef = useRef<InputValueReference>({ value: defaultValue });

  const [isFocused, setIsFocused] = useState(false);
  const [isFilled, setIsFilled] = useState(false);

  const handleInputFocus = useCallback(() => {
    setIsFocused(true);
  }, []);

  const handleInputBlur = useCallback(() => {
    setIsFocused(false);

    setIsFilled(!!inputValueRef.current.value);
  }, []);

  useImperativeHandle(ref, () => ({
    focus() {
      inputElementRef.current.focus();
    },
  }));

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputValueRef.current,
      path: 'value',
      setValue(refInput: any, value: string) {
        inputValueRef.current.value = value;
        inputElementRef.current.setNativeProps({ text: value });
      },
      clearValue() {
        inputValueRef.current.value = '';
        inputElementRef.current.clear();
      },
    });
  }, [fieldName, registerField]);

  return (
    <Container
      borderRadiusTop={borderRadiusTop}
      borderRadiusBottom={borderRadiusBottom}
      editable={editable}
    >
      {isFocused && <Indicator />}

      <Icon name={icon} size={18} isFocused={isFocused || isFilled} />

      <TextInput
        {...rest}
        ref={inputElementRef}
        placeholderTextColor={colors.additional}
        defaultValue={defaultValue}
        editable={editable}
        onFocus={handleInputFocus}
        onBlur={handleInputBlur}
        onChangeText={(value) => {
          inputValueRef.current.value = value;
        }}
      />

      {iconRight && (
        <ButtonRight onPress={onClickIconRight}>
          <IconRight name={iconRight} size={20} />
        </ButtonRight>
      )}
    </Container>
  );
};

export default forwardRef(Input);
