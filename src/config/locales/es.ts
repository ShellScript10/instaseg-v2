export default {
  welcome: {
    title: 'Bienvenido',
    subtitle: 'Elige dónde quieres continuar con tu experiencia',
    buttonLogin: 'Ya tienes una cuenta',
    buttonRegister: 'Crea una cuenta',
  },
  signIn: {
    title: 'Login',
    form: {
      registration: 'Registro',
      password: 'Contraseña',
      button: 'Iniciar sesión',
      forgotPassword: 'Olvide mi contraseña?',
    },
    validation: {
      registration: 'El registro es obligatorio',
      password: 'Se requiere contraseña',
      titleError: 'Error de autenticación',
      subtitleError:
        'Se produjo un error al iniciar sesión, verifique las credenciales',
    },
  },
  feed: {
    emptyMessage: `No se encontraron registros ${'\n'} en este momento 😢`,
  },
  search: {
    inputPlaceholder: 'Buscar...',
    emptyMessage: `No se encontraron registros ${'\n'} en este momento 😢`,
  },
  profile: {
    description: 'Haz clic para cambiar tu foto',
    form: {
      name: 'Nombre',
      registration: 'Registro',
      email: 'Correo electrónico',
      oldPassword: 'Contraseña anterior',
      newPassword: 'Nueva contraseña',
      button: 'Guardar información',
    },
    validation: {
      name: 'Se requiere el nombre',
      email: 'Correo electronico es requerido',
      titleError: 'Error al actualizar los datos',
      subtitleError:
        'Ocurrió un error durante la actualización, verifique los campos',
    },
  },
  user: {
    buttons: {
      profile: 'Mi perfil',
      notification: 'Notificaciones',
      signOut: 'Cerrar sesión',
    },
  },
  comments: {
    inputPlaceholder: 'Añadir un comentario',
    validation: {
      titleError: 'Error',
      subtitleError: 'Hubo un problema al registrar tu comentario',
    },
  },
  camera: {
    permission: {
      message: `Permiso para acceder a la cámara,${'\n'} no permitido`,
      button: 'Autorizar',
    },
    validation: {
      titleError: 'Error',
      subtitleError: 'No se pudo registrar tu foto',
    },
  },
  post: {
    postedBy: 'Publicado por',
    toBeContinued: 'continuará',
  },
  auth: {
    signIn: {
      validation: {
        titleError: 'Error de autenticación',
        subtitleError:
          'Se produjo un error al iniciar sesión, verifique las credenciales',
      },
    },
    updateUser: {
      validation: {
        titleError: 'Error de actualización',
        userError:
          'No se puede editar el usuario. Por favor, inténtelo de nuevo más tarde',
        passwordError: 'La contraseña ingresada no coincide con la registrada',
      },
    },
    updateUserPhoto: {
      validation: {
        titleError: 'Error',
        subtitleError: 'No pudimos actualizar tu foto de perfil',
      },
    },
  },
};
