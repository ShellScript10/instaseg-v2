import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import TabsRoutes from './tabs.routes';

import Comments from '../pages/Comments';
import Profile from '../pages/Profile';
import Notifications from '../pages/Notifications';

const App = createStackNavigator();

const AppRoutes: React.FC = () => (
  <App.Navigator
    screenOptions={{
      headerShown: false,
    }}
    initialRouteName="Home"
  >
    <App.Screen name="Home" component={TabsRoutes} />
    <App.Screen name="Comments" component={Comments} />
    <App.Screen name="Profile" component={Profile} />
    <App.Screen name="Notifications" component={Notifications} />
  </App.Navigator>
);

export default AppRoutes;
