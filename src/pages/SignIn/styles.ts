import styled from 'styled-components/native';
import { BorderlessButton, RectButton } from 'react-native-gesture-handler';

export const Container = styled.KeyboardAvoidingView`
  background: ${(props) => props.theme.colors.card};

  flex: 1;
`;

export const Header = styled.View`
  background: ${(props) => props.theme.colors.primary};

  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const Main = styled.View`
  background: ${(props) => props.theme.colors.card};
  padding: 32px 24px;
`;

export const Title = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 24px;
  color: ${(props) => props.theme.colors.title};
  margin-bottom: 32px;
`;

export const ButtonLogin = styled(RectButton)`
  height: 56px;
  border-radius: 8px;
  background: ${(props) => props.theme.colors.secondary};
  margin: 32px 0;

  align-items: center;
  justify-content: center;
`;

export const ButtonLoginText = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 16px;
  color: ${(props) => props.theme.colors.white};
`;

export const ButtonForgotPasswordText = styled.Text`
  font-family: Lato_400Regular;
  font-size: 12px;
  color: ${(props) => props.theme.colors.additional};
`;
