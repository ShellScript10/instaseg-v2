import styled, { css } from 'styled-components/native';
import { Dimensions, Platform } from 'react-native';

const { width } = Dimensions.get('window');

export const Container = styled.View`
  background: ${(props) => props.theme.colors.background};

  flex: 1;
`;

export const Header = styled.View`
  height: ${Platform.OS === 'ios' ? 120 : 80}px;
  background: ${(props) => props.theme.colors.primary};

  ${Platform.OS === 'ios'
    ? css`
        padding: 59px 24px 16px;
      `
    : css`
        padding: 16px 24px;
      `}

  justify-content: flex-end;
`;

export const Photo = styled.Image`
  width: ${width / 3}px;
  height: 144px;
`;

export const EmptyMessage = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 12px;
  color: ${(props) => props.theme.colors.primary};
  text-align: center;
  margin-top: 16px;

  align-self: center;
`;
