import React, { useCallback, useState } from 'react';
import {
  Animated,
  Dimensions,
  FlatList,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';

import Translation from '../../utils/getTranslation';

import {
  Container,
  Title,
  Zone,
  Branch,
  ContainerVideo,
  Video,
  ButtonReStart,
  IconPlay,
  Photo,
  Dots,
  Dot,
  Actions,
  ButtonViews,
  ButtonComments,
  Icon,
  Number,
  User,
  Avatar,
  NameText,
  Name,
  Description,
  More,
  HashTags,
  HashTag,
  Time,
} from './styles';

export interface PostItem {
  id: number;
  title: string;
  description: string;
  zone: string;
  branch: string;
  images: Array<string>;
  views: string;
  comments: string;
  user: {
    name: string;
    avatar_url: string;
  };
  hashtags: Array<string>;
  created_at: string;
}

interface PostProps {
  data: PostItem;
  onNavigate?: (data: PostItem) => void;
  details?: boolean;
}

const Post: React.FC<PostProps> = ({ data, onNavigate, details }) => {
  const { navigate } = useNavigation();

  const { width } = Dimensions.get('window');

  const [mutedVideo, setMutedVideo] = useState(true);

  const scrollX = new Animated.Value(0);
  const position = Animated.divide(scrollX, width);

  const handleNavigateCommentsPage = useCallback((post: PostItem) => {
    if (onNavigate) {
      onNavigate(post);
    }

    navigate('Comments', { post });
  }, []);

  return (
    <Container>
      <Title>{data.title}</Title>

      <Zone>{data.zone}</Zone>

      <Branch>{data.branch}</Branch>

      <FlatList
        data={data.images}
        keyExtractor={(item) => item}
        renderItem={({ item }) =>
          item.substr(-3) === 'mp4' ? (
            <TouchableWithoutFeedback
              onPress={() => setMutedVideo(!mutedVideo)}
            >
              <ContainerVideo>
                <Video
                  source={{ uri: item }}
                  rate={1.0}
                  volume={1.0}
                  isMuted={mutedVideo}
                  resizeMode="cover"
                  shouldPlay
                  isLooping
                />

                <ButtonReStart>
                  <IconPlay
                    name={mutedVideo ? 'volume-x' : 'volume-2'}
                    size={16}
                  />
                </ButtonReStart>
              </ContainerVideo>
            </TouchableWithoutFeedback>
          ) : (
            <Photo
              source={{
                uri: item,
              }}
            />
          )
        }
        horizontal
        showsHorizontalScrollIndicator={false}
        pagingEnabled
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: false },
        )}
      />

      {data.images.length > 1 && (
        <Dots>
          {data.images.map((dot, i) => {
            const opacity = position.interpolate({
              inputRange: [i - 1, i, i + 1],
              outputRange: [0.3, 1, 0.3],
              extrapolate: 'clamp',
            });

            return <Dot key={String(dot)} style={{ opacity }} />;
          })}
        </Dots>
      )}

      <Actions>
        <ButtonViews>
          <Icon name="eye" size={22} />

          <Number>{data.views}</Number>
        </ButtonViews>

        <ButtonComments onPress={() => handleNavigateCommentsPage(data)}>
          <Icon name="message-circle" size={22} />

          <Number>{data.comments}</Number>
        </ButtonComments>
      </Actions>

      <User>
        <Avatar source={{ uri: data.user.avatar_url }} />

        <View>
          <NameText>{Translation.t('post.postedBy')}</NameText>

          <Name>{data.user.name}</Name>
        </View>
      </User>

      {!details ? (
        <Description>
          {`${data.description.substring(0, 200)}... `}
          <More onPress={() => handleNavigateCommentsPage(data)}>
            {Translation.t('post.toBeContinued')}
          </More>
        </Description>
      ) : (
        <Description>{data.description}</Description>
      )}

      <HashTags>
        {data.hashtags.map((hashtag) => (
          <HashTag key={hashtag}>#{hashtag}</HashTag>
        ))}
      </HashTags>

      <Time>{data.created_at}</Time>
    </Container>
  );
};

export default Post;
