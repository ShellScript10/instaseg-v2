import styled, { css } from 'styled-components/native';
import { Platform } from 'react-native';

export const Container = styled.View`
  background: ${(props) => props.theme.colors.background};

  flex: 1;
`;

export const Header = styled.View`
  height: ${Platform.OS === 'ios' ? 104 : 80}px;
  background: ${(props) => props.theme.colors.primary};

  ${Platform.OS === 'ios' &&
  css`
    padding: 59px 0 32px;
  `}

  align-items: center;
  justify-content: center;
`;

export const EmptyMessage = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 12px;
  color: ${(props) => props.theme.colors.primary};
  text-align: center;

  align-self: center;
`;
