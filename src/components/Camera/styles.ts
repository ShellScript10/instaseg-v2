import styled, { css } from 'styled-components/native';
import { Platform } from 'react-native';
import { BorderlessButton, RectButton } from 'react-native-gesture-handler';
import { Feather } from '@expo/vector-icons';

export const ContainerNotPermission = styled.View`
  background: #000;
  padding: 0 24px;

  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const NotPermissionMessage = styled.Text`
  font-family: Lato_400Regular;
  font-size: 16px;
  color: ${(props) => props.theme.colors.white};
  text-align: center;
`;

export const ButtonAuthorize = styled(RectButton)`
  width: 100%;
  height: 56px;
  border-radius: 8px;
  background: ${(props) => props.theme.colors.secondary};
  margin: 32px 0;

  align-items: center;
  justify-content: center;
`;

export const ButtonAuthorizeText = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 16px;
  color: ${(props) => props.theme.colors.white};
`;

export const Container = styled.View`
  background: #000;

  flex: 1;
`;

export const Header = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  height: ${Platform.OS === 'ios' ? 104 : 80}px;
  background: rgba(0, 0, 0, 0.1);
  padding: 0 24px;

  ${Platform.OS === 'ios' &&
  css`
    padding: 32px 24px 0;
  `}

  justify-content: center;
`;

export const ButtonGoBack = styled.TouchableOpacity`
  width: 32px;
  height: 32px;
  background: ${(props) => props.theme.colors.background};
  border-radius: 8px;

  align-items: center;
  justify-content: center;
`;

export const IconGoBack = styled(Feather)`
  color: ${(props) => props.theme.colors.primary};
`;

export const Footer = styled.View`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.1);
  padding: 32px 48px;

  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const ButtonIcon = styled.TouchableOpacity``;

export const Icon = styled(Feather)`
  color: ${(props) => props.theme.colors.background};
`;

export const Picture = styled.TouchableOpacity`
  width: 72px;
  height: 72px;
  border-radius: 36px;
  border-width: 4px;
  border-color: ${(props) => props.theme.colors.background};

  align-items: center;
  justify-content: center;
`;
