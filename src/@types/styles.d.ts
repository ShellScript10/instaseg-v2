import 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    title: string;
    colors: {
      primary: string;
      secondary: string;
      background: string;
      card: string;
      border: string;
      additional: string;
      title: string;
      text: string;
      white: string;
      alert: string;
      success: string;
      info: string;
    };
  }
}
