import React from 'react';
import { StatusBar, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { ThemeProvider } from 'styled-components';
import { useFonts, Archivo_700Bold } from '@expo-google-fonts/archivo';
import { Lato_400Regular, Lato_700Bold } from '@expo-google-fonts/lato';

import AppProvader from './hooks';

import Routes from './routes';

import light from './themes/light';

const App: React.FC = () => {
  const [fontsLoaded] = useFonts({
    Archivo_700Bold,
    Lato_400Regular,
    Lato_700Bold,
  });

  if (!fontsLoaded) {
    return <View />;
  }

  return (
    <NavigationContainer>
      <StatusBar barStyle="light-content" backgroundColor="#7d1c1e" />

      <ThemeProvider theme={light}>
        <AppProvader>
          <Routes />
        </AppProvader>
      </ThemeProvider>
    </NavigationContainer>
  );
};
export default App;
