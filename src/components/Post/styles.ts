import styled from 'styled-components/native';
import { Dimensions, Animated } from 'react-native';
import { BorderlessButton } from 'react-native-gesture-handler';
import { Video as VideoAv } from 'expo-av';
import { Feather } from '@expo/vector-icons';

const { width } = Dimensions.get('window');

export const Container = styled.View`
  background: ${(props) => props.theme.colors.card};
  padding: 16px 0;
  margin-bottom: 16px;
`;

export const Title = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 16px;
  color: ${(props) => props.theme.colors.title};
  margin: 0 24px;
`;

export const Zone = styled.Text`
  font-family: Lato_400Regular;
  font-size: 12px;
  color: ${(props) => props.theme.colors.secondary};
  margin: 3px 24px 8px;
`;

export const Branch = styled.Text`
  font-family: Lato_400Regular;
  font-size: 12px;
  color: ${(props) => props.theme.colors.additional};
  margin: 0 24px;
`;

export const ContainerVideo = styled.View`
  width: ${width}px;
  height: 376px;
  margin: 16px 0;
`;

export const Video = styled(VideoAv)`
  width: ${width}px;
  height: 376px;
`;

export const ButtonReStart = styled.View`
  position: absolute;
  bottom: 16px;
  right: 16px;
  width: 32px;
  height: 32px;
  border-radius: 16px;
  background: rgba(0, 0, 0, 0.2);

  align-items: center;
  justify-content: center;
`;

export const IconPlay = styled(Feather)`
  color: ${(props) => props.theme.colors.white};
`;

export const Photo = styled.Image`
  width: ${width}px;
  height: 376px;
  margin: 16px 0;
`;

export const Dots = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const Dot = styled(Animated.View)`
  height: 4px;
  width: 4px;
  background-color: ${(props) => props.theme.colors.primary};
  border-radius: 2px;
  margin-right: 4px;
`;

export const Actions = styled.View`
  margin: 8px 24px 0;

  flex-direction: row;
  align-items: center;
`;

export const ButtonViews = styled.View`
  margin-right: 16px;

  flex-direction: row;
  align-items: center;
`;

export const ButtonComments = styled(BorderlessButton)`
  flex-direction: row;
  align-items: center;
`;

export const Icon = styled(Feather)`
  color: ${(props) => props.theme.colors.title};
  margin-right: 8px;
`;

export const Number = styled(Feather)`
  font-family: Archivo_700Bold;
  font-size: 14px;
  color: ${(props) => props.theme.colors.title};
`;

export const User = styled.View`
  margin: 16px 24px;

  flex-direction: row;
  align-items: center;
`;

export const Avatar = styled.Image`
  width: 24px;
  height: 24px;
  border-radius: 12px;
  margin-right: 12px;
`;

export const NameText = styled.Text`
  font-family: Lato_400Regular;
  font-size: 12px;
  color: ${(props) => props.theme.colors.additional};
  margin-bottom: 3px;
`;

export const Name = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 14px;
  color: ${(props) => props.theme.colors.title};
`;

export const Description = styled.Text`
  font-family: Lato_400Regular;
  font-size: 14px;
  color: ${(props) => props.theme.colors.text};
  line-height: 22px;
  margin: 0 24px 16px;

  flex: 1;
`;

export const More = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 14px;
  color: ${(props) => props.theme.colors.secondary};
`;

export const HashTags = styled.View`
  margin: 0 24px;

  flex-direction: row;
`;

export const HashTag = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 14px;
  color: ${(props) => props.theme.colors.secondary};
  margin-right: 8px;
`;

export const Time = styled.Text`
  font-family: Lato_400Regular;
  font-size: 12px;
  color: ${(props) => props.theme.colors.additional};
  margin: 16px 24px 0;
`;
