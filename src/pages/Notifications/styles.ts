import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';
import { Feather } from '@expo/vector-icons';

export const Container = styled.View`
  background: ${(props) => props.theme.colors.background};

  flex: 1;
`;

export const Header = styled.View`
  height: 104px;
  background: ${(props) => props.theme.colors.primary};
  padding: 32px 24px 0;

  justify-content: center;
`;

export const Icon = styled(Feather)`
  color: ${(props) => props.theme.colors.white};
`;

export const Notification = styled(RectButton)`
  background: ${(props) => props.theme.colors.card};
  padding: 16px 24px;
  margin-bottom: 16px;

  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Information = styled.View`
  margin: 0 16px;

  flex: 1;
`;

export const Avatar = styled.Image`
  width: 40px;
  height: 40px;
  border-radius: 20px;
`;

export const Description = styled.Text`
  font-family: Lato_400Regular;
  font-size: 14px;
  color: ${(props) => props.theme.colors.text};
`;

export const Name = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 14px;
  color: ${(props) => props.theme.colors.primary};
`;

export const Timer = styled.Text`
  font-family: Lato_400Regular;
  font-size: 12px;
  color: ${(props) => props.theme.colors.additional};
  margin-top: 8px;
`;

export const Photo = styled.Image`
  width: 40px;
  height: 40px;
  border-radius: 8px;
`;
