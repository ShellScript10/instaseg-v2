import React from 'react';

import App from './src';

const Bootstrap: React.FC = () => <App />;

export default Bootstrap;
