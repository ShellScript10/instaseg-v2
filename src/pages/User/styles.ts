import styled from 'styled-components/native';
import { BorderlessButton, RectButton } from 'react-native-gesture-handler';
import { Feather } from '@expo/vector-icons';

export const Container = styled.View`
  background: ${(props) => props.theme.colors.background};

  flex: 1;
`;

export const Header = styled.View`
  height: 200px;
  background: ${(props) => props.theme.colors.primary};
`;

export const UserInformations = styled.View`
  margin-top: -60px;
  margin-bottom: 32px;

  align-items: center;
`;

export const Avatar = styled.Image`
  width: 120px;
  height: 120px;
  background: ${(props) => props.theme.colors.additional};
  border-width: 4px;
  border-color: ${(props) => props.theme.colors.background};
  border-radius: 60px;
`;

export const Name = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 24px;
  color: ${(props) => props.theme.colors.title};
  margin-top: 16px;
`;

export const Role = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 14px;
  color: ${(props) => props.theme.colors.primary};
  margin: 8px 0 3px;
`;

export const Zone = styled.Text`
  font-family: Lato_400Regular;
  font-size: 12px;
  color: ${(props) => props.theme.colors.additional};
`;

export const Actions = styled.View`
  border-top-width: 1px;
  border-top-color: ${(props) => props.theme.colors.border};
  border-bottom-width: 1px;
  border-bottom-color: ${(props) => props.theme.colors.border};
  padding: 32px 0 16px;
  margin: 0 24px;
`;

export const Button = styled(RectButton)`
  height: 56px;
  background: ${(props) => props.theme.colors.secondary};
  border-radius: 8px;
  padding: 0 16px;
  margin-bottom: 16px;

  flex-direction: row;
  align-items: center;
`;

export const Icon = styled(Feather)`
  color: ${(props) => props.theme.colors.white};
`;

export const Text = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 16px;
  color: ${(props) => props.theme.colors.white};
  margin-left: 16px;
`;

export const ButtonOutline = styled.TouchableOpacity`
  height: 56px;
  border-width: 1px;
  border-color: ${(props) => props.theme.colors.secondary};
  border-radius: 8px;
  padding: 0 16px;
  margin: 32px 24px 0;

  flex-direction: row;
  align-items: center;
`;

export const IconOutline = styled(Feather)`
  color: ${(props) => props.theme.colors.secondary};
`;

export const TextOutline = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 16px;
  color: ${(props) => props.theme.colors.secondary};
  margin-left: 16px;
`;
