const shuffleArray = (array: Array<any>): Array<any> => {
  for (let i = array.length - 1; i > 0; i -= i) {
    const j = Math.floor(Math.random() * (i + 1));

    const temp = array[i];
    array[i] = array[j];
    array[j] = array[temp];
  }

  return array;
};

export default shuffleArray;
