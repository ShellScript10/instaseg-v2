import styled, { css } from 'styled-components/native';
import { Platform } from 'react-native';
import { BorderlessButton, RectButton } from 'react-native-gesture-handler';
import { Feather } from '@expo/vector-icons';

export const Container = styled.View`
  background: ${(props) => props.theme.colors.background};

  flex: 1;
`;

export const Header = styled.View`
  height: ${Platform.OS === 'ios' ? 104 : 80}px;
  background: ${(props) => props.theme.colors.primary};
  padding: 0 24px;

  ${Platform.OS === 'ios' &&
  css`
    padding: 32px 24px 0;
  `}

  justify-content: center;
`;

export const ButtonGoBack = styled(RectButton)`
  width: 32px;
  height: 32px;
  background: ${(props) => props.theme.colors.background};
  border-radius: 8px;

  align-items: center;
  justify-content: center;
`;

export const IconGoBack = styled(Feather)`
  color: ${(props) => props.theme.colors.primary};
`;

export const Footer = styled.View`
  height: 104px;
  background: ${(props) => props.theme.colors.card};
  box-shadow: 0 -0.5px 1px ${(props) => props.theme.colors.border};
  padding: 0 24px;

  flex-direction: row;
  align-items: center;
`;

export const ButtonCamera = styled(BorderlessButton)`
  margin-right: 16px;
`;

export const Icon = styled(Feather)`
  color: ${(props) => props.theme.colors.secondary};
`;

export const HeaderCamera = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  height: 104px;
  background: rgba(0, 0, 0, 0.1);
  padding: 32px 24px 0;

  justify-content: center;
`;

export const FooterCamera = styled.View`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.1);
  padding: 32px 48px;

  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Picture = styled(BorderlessButton)`
  width: 72px;
  height: 72px;
  border-radius: 36px;
  border-width: 4px;
  border-color: ${(props) => props.theme.colors.background};

  align-items: center;
  justify-content: center;
`;
