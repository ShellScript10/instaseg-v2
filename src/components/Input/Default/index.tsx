import React, { useContext } from 'react';
import { TextInputProps } from 'react-native';
import { ThemeContext } from 'styled-components';

import { Container, Icon, Input } from './styles';

interface InputDefaultProps extends TextInputProps {
  showLeftIcon?: boolean;
}

const InputDefault: React.FC<InputDefaultProps> = ({
  showLeftIcon,
  ...rest
}) => {
  const { colors } = useContext(ThemeContext);

  return (
    <Container>
      {showLeftIcon && <Icon name="search" size={18} />}

      <Input {...rest} placeholderTextColor={colors.additional} />
    </Container>
  );
};

export default InputDefault;
