import { BorderlessButton } from 'react-native-gesture-handler';
import styled from 'styled-components/native';

export const Container = styled.View`
  background: ${(props) => props.theme.colors.card};
  padding: 16px 24px;

  flex-direction: row;
`;

export const Avatar = styled.Image`
  width: 32px;
  height: 32px;
  border-radius: 16px;
`;

export const Informations = styled.View`
  margin-left: 16px;
`;

export const Description = styled.Text`
  font-family: Lato_400Regular;
  font-size: 14px;
  color: ${(props) => props.theme.colors.text};
`;

export const Name = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 14px;
  color: ${(props) => props.theme.colors.title};
`;

export const ButtonPhoto = styled(BorderlessButton)``;

export const Photo = styled.Image`
  width: 104px;
  height: 176px;
  border-radius: 10px;
  margin-top: 8px;
`;

export const Time = styled.Text`
  font-family: Lato_400Regular;
  font-size: 12px;
  color: ${(props) => props.theme.colors.additional};
  margin-top: 16px;
`;
