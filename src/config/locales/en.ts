export default {
  welcome: {
    title: 'Welcome',
    subtitle: 'Choose where you want to continue with your experience',
    buttonLogin: 'Already have an account',
    buttonRegister: 'Create an account',
  },
  signIn: {
    title: 'Login',
    form: {
      registration: 'Registration',
      password: 'Password',
      button: 'Log in',
      forgotPassword: 'Forgot my password?',
    },
    validation: {
      registration: 'Registration is mandatory',
      password: 'Password is required',
      titleError: 'Authentication error',
      subtitleError: 'An error occurred while logging in, check credentials',
    },
  },
  feed: {
    emptyMessage: `No records found ${'\n'} at the moment 😢`,
  },
  search: {
    inputPlaceholder: 'Search...',
    emptyMessage: `No records found ${'\n'} at the moment 😢`,
  },
  profile: {
    description: 'Click to change your photo',
    form: {
      name: 'Name',
      registration: 'Registration',
      email: 'E-mail',
      oldPassword: 'Old password',
      newPassword: 'New password',
      button: 'Save information',
    },
    validation: {
      name: 'Name is required',
      email: 'E-mail is required',
      titleError: 'Error updating data',
      subtitleError:
        'An error occurred while updating, please check the fields',
    },
  },
  user: {
    buttons: {
      profile: 'My profile',
      notification: 'Notifications',
      signOut: 'Sign out',
    },
  },
  comments: {
    inputPlaceholder: 'Add a comment',
    validation: {
      titleError: 'Error',
      subtitleError: 'There was a problem registering your comment',
    },
  },
  camera: {
    permission: {
      message: `Permission to access the camera,${'\n'} not allowed`,
      button: 'Authorize',
    },
    validation: {
      titleError: 'Error',
      subtitleError: 'Couldn`t register your photo',
    },
  },
  post: {
    postedBy: 'Posted by',
    toBeContinued: 'continued',
  },
  auth: {
    signIn: {
      validation: {
        titleError: 'Authentication error',
        subtitleError: 'An error occurred while logging in, check credentials',
      },
    },
    updateUser: {
      validation: {
        titleError: 'Update error',
        userError: 'User cannot be edited. Please try again later',
        passwordError: 'Password entered does not match the registered one',
      },
    },
    updateUserPhoto: {
      validation: {
        titleError: 'Error',
        subtitleError: 'We were unable to update your profile photo',
      },
    },
  },
};
