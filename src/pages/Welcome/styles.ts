import styled from 'styled-components/native';
import { BorderlessButton, RectButton } from 'react-native-gesture-handler';

export const Container = styled.View`
  background: ${(props) => props.theme.colors.primary};

  flex: 1;
`;

export const Header = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const Flags = styled.View`
  margin-bottom: 16px;

  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const Main = styled.View`
  background: ${(props) => props.theme.colors.card};
  padding: 32px 24px 56px;

  justify-content: space-between;
`;

export const Title = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 24px;
  color: ${(props) => props.theme.colors.title};
`;

export const SubTitle = styled.Text`
  font-family: Lato_400Regular;
  font-size: 14px;
  color: ${(props) => props.theme.colors.additional};
  line-height: 22px;
  margin-top: 8px;
`;

export const Buttons = styled.View`
  margin-top: 32px;
`;

export const Button = styled(RectButton)`
  height: 56px;
  border-radius: 8px;
  background: ${(props) => props.theme.colors.secondary};

  align-items: center;
  justify-content: center;
`;

export const ButtonText = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 16px;
  color: ${(props) => props.theme.colors.white};
`;

export const ButtonRegister = styled(BorderlessButton)`
  margin-top: 16px;

  align-self: center;
`;

export const ButtonRegisterText = styled.Text`
  font-family: Archivo_700Bold;
  font-size: 14px;
  color: ${(props) => props.theme.colors.text};
`;
