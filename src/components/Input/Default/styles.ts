import styled from 'styled-components/native';
import { Feather } from '@expo/vector-icons';

export const Container = styled.View`
  height: 40px;
  background: ${(props) => props.theme.colors.background};
  border-radius: 8px;
  padding: 0 16px;

  flex: 1;
  flex-direction: row;
  align-items: center;
`;

export const Icon = styled(Feather)`
  color: ${(props) => props.theme.colors.additional};
  margin-right: 16px;
`;

export const Input = styled.TextInput`
  height: 100%;
  font-family: Lato_400Regular;
  font-size: 14px;
  color: ${(props) => props.theme.colors.text};

  flex: 1;
`;
